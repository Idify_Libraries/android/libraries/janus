package com.idfy.janus;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.opengl.GLES20;
import android.opengl.GLUtils;
import android.view.TextureView;

import com.idfy.core.DALCapture;

import org.webrtc.SurfaceTextureHelper;
import org.webrtc.TextureBufferImpl;
import org.webrtc.VideoCapturer;
import org.webrtc.VideoFrame;
import org.webrtc.YuvConverter;

import java.text.SimpleDateFormat;
import java.util.Date;

public class MyCustomVideoCapture implements VideoCapturer {

    private SurfaceTextureHelper surTexture;
    private Context appContext;
    private org.webrtc.CapturerObserver capturerObs;
    private Thread captureThread;
    private TextureView textureView;
    private boolean isVideoOverlayEnabled;
    private String latitude;
    private String longitude;
    private String strDate = "";
    private String strTime = "";

    @Override
    public void initialize(SurfaceTextureHelper surfaceTextureHelper, Context applicationContext, org.webrtc.CapturerObserver capturerObserver) {
        surTexture = surfaceTextureHelper;
        appContext = applicationContext;
        capturerObs = capturerObserver;
    }

    public MyCustomVideoCapture(TextureView textureView,boolean isVideoOverlayEnabled,String latitude,String longitude) {
        this.textureView = textureView;
        this.isVideoOverlayEnabled = isVideoOverlayEnabled;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    @Override
    public void startCapture(int width, int height, int fps) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        captureThread = new Thread(() -> {
            try {
                long start = System.nanoTime();
                capturerObs.onCapturerStarted(true);
                int[] textures = new int[1];
                GLES20.glGenTextures(1, textures, 0);
                YuvConverter yuvConverter = new YuvConverter();
                TextureBufferImpl buffer = new TextureBufferImpl(width, height, VideoFrame.TextureBuffer.Type.RGB, textures[0], new Matrix(), surTexture.getHandler(), yuvConverter, null);
                while (true) {
                    if (isVideoOverlayEnabled) {
                        Date date = new Date();
                        String dateAndTime = formatter.format(date);
                        String[] parts = dateAndTime.split(" ");
                        strDate = parts[0];
                        strTime = parts[1];
                    }
                    Bitmap bitmap1 = createFlippedBitmap(
                            strDate+"\n"+strTime+"\nLat,Long"+"\n"+latitude+", "+longitude,
                            textureView.getBitmap(bitmap),true,false);
                    surTexture.getHandler().post(() -> {
                        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_NEAREST);
                        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_NEAREST);
                        GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, bitmap1, 0);
                        VideoFrame.I420Buffer i420Buf = yuvConverter.convert(buffer);
                        long frameTime = System.nanoTime() - start;
                        VideoFrame videoFrame = new VideoFrame(i420Buf, 180, frameTime);
                        capturerObs.onFrameCaptured(videoFrame);
                        videoFrame.release();
                    });
                    Thread.sleep(100);
                }
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        });
        captureThread.start();
    }

    @Override
    public void stopCapture() {
        captureThread.interrupt();
        surTexture.stopListening();
        capturerObs.onCapturerStopped();
        surTexture.getHandler().removeCallbacks(null);
        surTexture.dispose();
        textureView = null;
    }

    @Override
    public void changeCaptureFormat(int width, int height, int fps) {
    }

    @Override
    public void dispose() {
    }

    @Override
    public boolean isScreencast() {
        return false;
    }

    private Bitmap createFlippedBitmap(String gText,Bitmap source, boolean xFlip, boolean yFlip) {
        try {
            Matrix matrix = new Matrix();
            matrix.postScale(xFlip ? -1 : 1, yFlip ? -1 : 1, source.getWidth() / 2f, source.getHeight() / 2f);
            if (isVideoOverlayEnabled) {
                Canvas canvas = new Canvas(source);
                Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
                paint.setColor(Color.WHITE);
                paint.setTextSize(20);
                Rect bounds = new Rect();
                paint.getTextBounds(gText, 0, gText.length(), bounds);
                int x = 10;
                int y = 25;
                for (String line : gText.split("\n")) {
                    canvas.drawText(line, x, y, paint);
                    y += paint.descent() - paint.ascent();
                }
            }
            return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
        } catch (Exception e) {
            return null;
        }
    }
}