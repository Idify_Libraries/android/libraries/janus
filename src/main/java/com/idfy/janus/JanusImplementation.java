package com.idfy.janus;

import android.content.Context;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.TextureView;

import com.google.gson.Gson;
import com.idfy.core.model.WebrtcStatsModel;

import org.json.JSONArray;
import org.json.JSONObject;
import org.webrtc.Camera1Enumerator;
import org.webrtc.Camera2Enumerator;
import org.webrtc.CameraEnumerator;
import org.webrtc.EglBase;
import org.webrtc.IceCandidate;
import org.webrtc.MediaStreamTrack;
import org.webrtc.PeerConnection;
import org.webrtc.RTCStatsReport;
import org.webrtc.SessionDescription;
import org.webrtc.SurfaceViewRenderer;
import org.webrtc.VideoCapturer;

import java.math.BigInteger;
import java.util.concurrent.ConcurrentHashMap;


/**
 * HELLO
 */


public class JanusImplementation implements JanusRTCInterface, PeerConnectionClient.PeerConnectionEvents {

    private static final String TAG = "MainActivity";
    private PeerConnectionClient peerConnectionClient;
    private PeerConnectionClient.PeerConnectionParameters peerConnectionParameters;
    private SurfaceViewRenderer localRender;
    private SurfaceViewRenderer remoteRender;
    private VideoCapturer videoCapturer;
    private EglBase.Context rootEglBase;
    private WebSocketChannel mWebSocketChannel;
    private Context context;
    private TextureView localTextureView;
    private String url;
    private String roomId;
    private JanusStateCallback janusStateCallback;
    private boolean isIceConnected = false;
    private CustomVideoCapture customVideoCapture;
    private JSONArray iceServerArray;
    private int reconnectingRoomId;
    private String streamId;
    private boolean isVideoOverlayEnabled;
    private String latitude;
    private String longitude;
    private WebrtcStatsCallback webrtcStatsCallback;
    private boolean isRemoteStreamReceived = false;
    private CountDownTimer playStartCountDownTimer = null;
    private CountDownTimer playNotStartedCountDownTimer = null;

    public JanusImplementation(Context context,EglBase.Context rootEglBase, TextureView localTextureView,
                               SurfaceViewRenderer remoteRender,
                               JSONArray iceServerArray,
                               boolean isVideoOverlayEnabled,String latitude,String longitude,
                               JanusStateCallback janusStateCallback,WebrtcStatsCallback webrtcStatsCallback) {
        this.rootEglBase = rootEglBase;
        this.remoteRender = remoteRender;
        this.janusStateCallback = janusStateCallback;
        this.localTextureView = localTextureView;
        this.iceServerArray = iceServerArray;
        this.isVideoOverlayEnabled = isVideoOverlayEnabled;
        this.latitude = latitude;
        this.longitude = longitude;
        this.context = context;
        this.webrtcStatsCallback = webrtcStatsCallback;
    }


    public void init(String url, String roomId, int reconnectingRoomId, String streamId,String agentStreamId) {
        this.roomId = roomId;
        this.reconnectingRoomId = reconnectingRoomId;
        this.streamId = streamId;
        isRemoteStreamReceived = false;
        mWebSocketChannel = new WebSocketChannel(url, roomId, streamId,reconnectingRoomId);
        mWebSocketChannel.initConnection();
        mWebSocketChannel.setDelegate(this);
        peerConnectionParameters = new PeerConnectionClient.PeerConnectionParameters(false, 360, 480, 20,
                "H264", true, 0, "opus",
                false, false, false, false, false,
                iceServerArray,streamId,agentStreamId,roomId,reconnectingRoomId);
        peerConnectionClient = PeerConnectionClient.getInstance();
        peerConnectionClient.createPeerConnectionFactory(context, peerConnectionParameters, this);
    }

    public boolean isSocketOpen() {
        return mWebSocketChannel.isSocketOpen();
    }

    public void hangUp() {
        if (peerConnectionClient != null) {
            peerConnectionClient.close();
        }
    }

    public void setAudio(boolean enable) {
        if (peerConnectionClient != null) {
            peerConnectionClient.setAudioEnabled(enable);
        }
    }

    public void setVideoPublish(boolean enable) {
        if (customVideoCapture != null) {
            customVideoCapture.setVideoPublish(enable);
        }
    }

    public void setVideoEnable(boolean enable) {
        if (peerConnectionClient != null) {
            peerConnectionClient.setVideoEnabled(enable);
        }
    }

    public boolean isVideoPublish() {
        boolean isPublish = false;
        if (customVideoCapture != null) {
            isPublish = customVideoCapture.isVideoPublish();
        }
        return isPublish;
    }

    public void leaveRoom() {
        if (mWebSocketChannel != null) {
            mWebSocketChannel.leaveRoom();
        }
    }

    public void destroyJanus() {
        if (mWebSocketChannel != null) {
            mWebSocketChannel.destroyJanus();
        }
    }

    public void joinRoom() {
        if (mWebSocketChannel != null) {
            mWebSocketChannel.publisherJoinRoom();
        }
    }


    public boolean isAudioEnabled() {
        return peerConnectionClient.isAudioEnable();
    }
    public String getAudioState() {
        return peerConnectionClient.getAudioState();
    }

    public boolean isVideoEnabled() {
        return peerConnectionClient.isVideoEnable();
    }
    public String getVideoState() {
        return peerConnectionClient.getVideoState();
    }

    public ConcurrentHashMap<BigInteger, JanusConnection> getPeerConnection(){
        if (peerConnectionClient != null) {
            return peerConnectionClient.getPeerConnection();
        }
        return null;
    }

    public PeerConnection getLocalPeerConnection(){
        if (peerConnectionClient != null){
           return peerConnectionClient.getLocalPeerConnection();
        }
        return null;
    }

    public PeerConnection getRemotePeerConnection(){
        if (peerConnectionClient != null){
            return peerConnectionClient.getRemotePeerConnection();
        }
        return null;
    }

    public void startCapture() {
        peerConnectionClient.startVideoSource();
    }

    private boolean useCamera2() {
        return Camera2Enumerator.isSupported(context);
    }

    private boolean captureToTexture() {
        return true;
    }

    private VideoCapturer createCameraCapturer(CameraEnumerator enumerator) {
        final String[] deviceNames = enumerator.getDeviceNames();

        // First, try to find front facing camera
        Log.d(TAG, "Looking for front facing cameras.");
        for (String deviceName : deviceNames) {
            if (enumerator.isFrontFacing(deviceName)) {
                Log.d(TAG, "Creating front facing camera capturer.");
                VideoCapturer videoCapturer = enumerator.createCapturer(deviceName, null);

                if (videoCapturer != null) {
                    return videoCapturer;
                }
            }
        }

        // Front facing camera not found, try something else
        Log.d(TAG, "Looking for other cameras.");
        for (String deviceName : deviceNames) {
            if (!enumerator.isFrontFacing(deviceName)) {
                Log.d(TAG, "Creating other camera capturer.");
                VideoCapturer videoCapturer = enumerator.createCapturer(deviceName, null);

                if (videoCapturer != null) {
                    return videoCapturer;
                }
            }
        }

        return null;
    }

    private VideoCapturer createVideoCapturer() {
        VideoCapturer videoCapturer = null;
        if (useCamera2()) {
            Log.d(TAG, "Creating capturer using camera2 API.");
            videoCapturer = createCameraCapturer(new Camera2Enumerator(context));
        } else {
            Log.d(TAG, "Creating capturer using camera1 API.");
            videoCapturer = createCameraCapturer(new Camera1Enumerator(captureToTexture()));
        }
        if (videoCapturer == null) {
            Log.e(TAG, "Failed to open camera");
            return null;
        }
        return videoCapturer;
    }
//    CustomVideoCapturer customVideoCapturer = new CustomVideoCapturer(autoFitTextureView, 20);
//    SurfaceTextureHelper surfaceTextureHelper = SurfaceTextureHelper.create("CaptureThread", eglBase.getEglBaseContext());
//    // source = sessionFactory.createVideoSource(false);
//        customVideoCapturer.initialize(surfaceTextureHelper, context, source.getCapturerObserver());
//        customVideoCapturer.startCapture(480, 640, 20);

    private void offerPeerConnection(BigInteger handleId) {
        // videoCapturer = createVideoCapturer();
       MyCustomVideoCapture myCustomVideoCapture = new MyCustomVideoCapture(localTextureView,
               isVideoOverlayEnabled,latitude,longitude);
       // customVideoCapture = new CustomVideoCapture(localTextureView, 20);
        peerConnectionClient.createPeerConnection(rootEglBase, myCustomVideoCapture, handleId);
        peerConnectionClient.createOffer(handleId);
    }

    // interface JanusRTCInterface
    @Override
    public void onPublisherJoined(final BigInteger handleId) {
        Log.d("=HANDLE", String.valueOf(handleId));
        offerPeerConnection(handleId);
    }

    @Override
    public void onPublisherRemoteJsep(BigInteger handleId, JSONObject jsep) {
        SessionDescription.Type type = SessionDescription.Type.fromCanonicalForm(jsep.optString("type"));
        String sdp = jsep.optString("sdp");
        SessionDescription sessionDescription = new SessionDescription(type, sdp);
        peerConnectionClient.setRemoteDescription(handleId, sessionDescription);
    }

    @Override
    public void subscriberHandleRemoteJsep(BigInteger handleId, JSONObject jsep) {
        SessionDescription.Type type = SessionDescription.Type.fromCanonicalForm(jsep.optString("type"));
        String sdp = jsep.optString("sdp");
        SessionDescription sessionDescription = new SessionDescription(type, sdp);
        peerConnectionClient.subscriberHandleRemoteJsep(handleId, sessionDescription);
    }

    @Override
    public void onLeaving(BigInteger handleId) {

    }

    @Override
    public void onVideoPublish(boolean isVideoPublish) {
        janusStateCallback.onPublishSuccessful(isVideoPublish,isRemoteStreamReceived, (isIceConnected) ? "connected" : "disconnected");
    }

    @Override
    public void onReconnectCall(String type, int roomId, String reason) {
        janusStateCallback.onReconnectCall(type,roomId,reason);
    }

    @Override
    public void onSubscriberAttached() {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                if (playNotStartedCountDownTimer == null) {
                    playNotStartedCountDownTimer = new CountDownTimer(BuildConfig.RECONNECTING_TIME, 1000) {
                        @Override
                        public void onTick(long millisUntilFinished) {

                        }

                        @Override
                        public void onFinish() {
                            onReconnectCall("auto", reconnectingRoomId, "PlayNotStarted");

                        }
                    }.start();
                }
            }
        });
    }

    // interface PeerConnectionClient.PeerConnectionEvents
    @Override
    public void onLocalDescription(SessionDescription sdp, BigInteger handleId) {
        Log.e(TAG, sdp.type.toString());
        mWebSocketChannel.publisherCreateOffer(handleId, sdp);
    }

    @Override
    public void onRemoteDescription(SessionDescription sdp, BigInteger handleId) {
        Log.e(TAG, sdp.type.toString());
        mWebSocketChannel.subscriberCreateAnswer(handleId, sdp);
    }

    @Override
    public void onIceCandidate(IceCandidate candidate, BigInteger handleId) {
        Log.e(TAG, "=========onIceCandidate========");
        if (candidate != null) {
            mWebSocketChannel.trickleCandidate(handleId, candidate);
        } else {
            mWebSocketChannel.trickleCandidateComplete(handleId);
        }
    }

    @Override
    public void onIceCandidatesRemoved(IceCandidate[] candidates) {

    }

    @Override
    public void onIceConnected() {
        isIceConnected = true;
    }

    @Override
    public void onIceDisconnected() {
        janusStateCallback.onIceSateFailed("Disconnected");
    }

    @Override
    public void onPeerConnectionClosed() {
        Log.d("===SOC","peer connection CLOSED");
        if (playStartCountDownTimer != null){
            playStartCountDownTimer.cancel();
            playStartCountDownTimer = null;
        }
        if (playNotStartedCountDownTimer != null){
            playNotStartedCountDownTimer.cancel();
            playNotStartedCountDownTimer = null;
        }
        mWebSocketChannel.socketClose();
    }

    @Override
    public void onPeerConnectionStatsReady(RTCStatsReport reports) {
        //  Log.d("====STATS",reports.toString());
        //longInfo("==RTC Stats: \n" + reports.toString());

    }

    @Override
    public void onPeerConnectionLocalStatsReady(WebrtcStatsModel webrtcStatsModel) {
        Log.d("==STATS","LOCAL "+new Gson().toJson(webrtcStatsModel));
        webrtcStatsCallback.onWebrtcStatsReceived(webrtcStatsModel);
    }

    @Override
    public void onPeerConnectionRemoteStatsReady(WebrtcStatsModel  webrtcStatsModel) {
        Log.d("==STATS","REMOTE "+new Gson().toJson(webrtcStatsModel));
        webrtcStatsCallback.onWebrtcStatsReceived(webrtcStatsModel);
    }



    @Override
    public void onPeerConnectionError(String description) {

    }

    @Override
    public void onRemoteRender(final JanusConnection connection) {
        if (connection != null) {
            connection.videoTrack.addSink(remoteRender);
            isRemoteStreamReceived = true;
            if (playStartCountDownTimer != null){
                playStartCountDownTimer.cancel();
                playStartCountDownTimer = null;
            }
            if (playNotStartedCountDownTimer != null){
                playNotStartedCountDownTimer.cancel();
                playNotStartedCountDownTimer = null;
            }
        }else {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    if (playStartCountDownTimer == null) {
                        playStartCountDownTimer = new CountDownTimer(BuildConfig.RECONNECTING_TIME, 1000) {
                            @Override
                            public void onTick(long millisUntilFinished) {

                            }

                            @Override
                            public void onFinish() {
                                onReconnectCall("auto", reconnectingRoomId, "PlayNotResume");

                            }
                        }.start();
                    }
                }
            });
        }

    }

    public void longInfo(String str, String tag) {
        if (str.length() > 4000) {
            Log.i(tag, str.substring(0, 4000));
            longInfo(str.substring(4000), tag);
        } else
            Log.i(tag, str);
    }

}
