package com.idfy.janus;

import com.idfy.core.model.WebrtcStatsModel;

public interface WebrtcStatsCallback {
    void onWebrtcStatsReceived(WebrtcStatsModel webrtcStatsModel);
}
