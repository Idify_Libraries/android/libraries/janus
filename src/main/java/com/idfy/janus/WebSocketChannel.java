package com.idfy.janus;

import android.os.CountDownTimer;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Log;


import com.koushikdutta.async.ByteBufferList;
import com.koushikdutta.async.DataEmitter;
import com.koushikdutta.async.callback.CompletedCallback;
import com.koushikdutta.async.callback.DataCallback;
import com.koushikdutta.async.callback.WritableCallback;
import com.koushikdutta.async.http.AsyncHttpClient;
import com.koushikdutta.async.http.WebSocket;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.webrtc.IceCandidate;
import org.webrtc.SessionDescription;

import java.math.BigInteger;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;


public class WebSocketChannel {
    private static final String TAG = "WebSocketChannel";

    private WebSocket mWebSocket;
    private ConcurrentHashMap<String, JanusTransaction> transactions = new ConcurrentHashMap<>();
    private ConcurrentHashMap<BigInteger, JanusHandle> handles = new ConcurrentHashMap<>();
    private ConcurrentHashMap<String, JanusHandle> feeds = new ConcurrentHashMap<>();
    private Handler mHandler;
    private BigInteger mSessionId;
    private JanusRTCInterface delegate;
    private String roomId;
    private String url;
    private HandlerThread handlerThread;
    private String streamId;
    private int reconnectingId;
    private BigInteger handleId;
    private CountDownTimer publishStartCountDownTimer = null;
    private CountDownTimer publishResumeCountDownTimer = null;

    public WebSocketChannel(String url,String roomId,String streamId,int reconnectingId) {
        this.url = url;
        this.roomId = roomId;
        this.streamId = streamId;
        this.reconnectingId = reconnectingId;
        handlerThread = new HandlerThread("KeepALiveThread");
        handlerThread.start();
        mHandler = new Handler(handlerThread.getLooper());
    }


    public boolean isSocketOpen(){
       return mWebSocket.isOpen();
    }
    public void socketClose(){
        //handlerThread.quitSafely();
        if (publishStartCountDownTimer != null) {
            publishStartCountDownTimer.cancel();
            publishStartCountDownTimer = null;
        }
        if (publishResumeCountDownTimer != null){
            publishResumeCountDownTimer.cancel();
            publishResumeCountDownTimer = null;
        }
        mHandler.removeCallbacks(null);
        mWebSocket.close();
    }

    public void initConnection() {
//        OkHttpClient httpClient = new OkHttpClient.Builder()
//                .addNetworkInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
//                .addInterceptor(new Interceptor() {
//                    @Override
//                    public Response intercept(Interceptor.Chain chain) throws IOException {
//                        Request.Builder builder = chain.request().newBuilder();
//                        builder.addHeader("Sec-WebSocket-Protocol", "janus-protocol");
//                        return chain.proceed(builder.build());
//                    }
//                }).connectTimeout(10, TimeUnit.SECONDS)
//                .readTimeout(10, TimeUnit.SECONDS)
//                .build();
//        Request request = new Request.Builder().url(url).build();
//        mWebSocket = httpClient.newWebSocket(request, new WebSocketListener() {
//            @Override
//            public void onOpen(WebSocket webSocket, Response response) {
//                Log.e(TAG, "onOpen");
//                createSession();
//            }
//
//            @Override
//            public void onMessage(WebSocket webSocket, String text) {
//                Log.e(TAG, "onMessage");
//                WebSocketChannel.this.onMessage(text);
//            }
//
//            @Override
//            public void onMessage(WebSocket webSocket, ByteString bytes) {
//            }
//
//            @Override
//            public void onClosing(WebSocket webSocket, int code, String reason) {
//                Log.e(TAG, "onClosing");
//            }
//
//            @Override
//            public void onClosed(WebSocket webSocket, int code, String reason) {
//            }
//
//            @Override
//            public void onFailure(WebSocket webSocket, Throwable t, Response response) {
//                Log.e(TAG, "onFailure" + t.toString());
//            }
//        });
        AsyncHttpClient.getDefaultInstance().websocket(url, "janus-protocol", new AsyncHttpClient.WebSocketConnectCallback() {
            @Override
            public void onCompleted(Exception ex, com.koushikdutta.async.http.WebSocket webSocket) {
                if (ex != null) {
                    //handler.onError(ex);
                }
                mWebSocket = webSocket;
                createSession();
                mWebSocket.setWriteableCallback(new WritableCallback() {
                    @Override
                    public void onWriteable() {
                        Log.d("JANUSCLIENT", "On writable");
                    }
                });
                mWebSocket.setPongCallback(new WebSocket.PongCallback() {

                    @Override
                    public void onPongReceived(String s) {
                        Log.d("JANUSCLIENT", "Pong callback");
                    }
                });
                mWebSocket.setDataCallback(new DataCallback() {

                    @Override
                    public void onDataAvailable(DataEmitter emitter, ByteBufferList bb) {
                        Log.d("JANUSCLIENT", "New Data");
                    }
                });
                mWebSocket.setEndCallback(new CompletedCallback() {

                    @Override
                    public void onCompleted(Exception ex) {
                        Log.d("JANUSCLIENT", "Client End");
                    }
                });
                mWebSocket.setStringCallback(new WebSocket.StringCallback() {
                    @Override
                    public void onStringAvailable(String s) {
                        onMessage(s);
                    }
                });
                mWebSocket.setClosedCallback(new CompletedCallback() {
                    @Override
                    public void onCompleted(Exception ex) {
                        Log.d("JANUSCLIENT", "Socket closed for some reason");
//                        if (ex != null) {
//                            Log.d("JANUSCLIENT", "SOCKET EX " + ex.getMessage());
//                            StringWriter writer = new StringWriter();
//                            PrintWriter printWriter = new PrintWriter( writer );
//                            ex.printStackTrace( printWriter );
//                            printWriter.flush();
//                            Log.d("JANUSCLIENT", "StackTrace \n\t" + writer.toString());
//                        }
//                        if (ex != null) {
//                            onError(ex);
//                        } else {
//                            onClose(-1, "unknown", true);
//                        }
                    }
                });


            }
        });
    }

    private void onMessage(String message) {
        Log.e(TAG, "onMessage" + message);
        try {
            JSONObject jo = new JSONObject(message);
            String janus = jo.optString("janus");
            if (janus.equals("success")) {
                String transaction = jo.optString("transaction");
                JanusTransaction jt = transactions.get(transaction);
                if (jt != null) {
                    if (jt.success != null) {
                        jt.success.success(jo);
                    }
                }
                transactions.remove(transaction);
            } else if (janus.equals("error")) {
                String transaction = jo.optString("transaction");
                JanusTransaction jt = transactions.get(transaction);
                if (jt != null) {
                    if (jt.error != null) {
                        jt.error.error(jo);
                    }
                }
                transactions.remove(transaction);
            } else if (janus.equals("ack")) {
                Log.e(TAG, "Just an ack");
            } else if (janus.equals("media")){
                    if (jo.has("type")){
                        String videType  = jo.getString("type");
                        if (videType.equals("video")) {
                            boolean isVideoReceived = jo.getBoolean("receiving");
                            if (isVideoReceived) {
                                // TODO: 01/11/21
                                // If timer not equal to null then cancel the timer and set it to null
                                if (publishStartCountDownTimer != null) {
                                    publishStartCountDownTimer.cancel();
                                    publishStartCountDownTimer = null;
                                }
                                if (publishResumeCountDownTimer != null){
                                    publishResumeCountDownTimer.cancel();
                                    publishResumeCountDownTimer = null;
                                }
                                if (delegate != null) {
                                    delegate.onVideoPublish(true);
                                }
                            }else {
                                // TODO: 01/11/21
                                //set call back to show connecting loader
                                // if timer is null then only set timmer
                                // set a timer for 10 sec to fire reconnecting event
                                new Handler(Looper.getMainLooper()).post(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (publishStartCountDownTimer == null && publishResumeCountDownTimer == null) {
                                            publishResumeCountDownTimer = new CountDownTimer( BuildConfig.RECONNECTING_TIME, 1000) {
                                                @Override
                                                public void onTick(long millisUntilFinished) {

                                                }

                                                public void onFinish() {
                                                    delegate.onReconnectCall("auto",reconnectingId,"PublishDidNotResume");
                                                }

                                            }.start();
                                        }
                                    }
                                });
                            }
                        }
                    }

            }
            else {
                JanusHandle handle = handles.get(new BigInteger(jo.optString("sender")));
                if (handle == null) {
                    Log.e(TAG, "missing handle");
                } else if (janus.equals("event")) {
                    JSONObject plugin = jo.optJSONObject("plugindata").optJSONObject("data");
                    if (plugin.optString("videoroom").equals("joined")) {
                        handle.onJoined.onJoined(handle);
                    }
                    if (plugin.optString("videoroom").equals("attached")){
                        delegate.onSubscriberAttached();
                    }
                    if (plugin.has("error_code")){
                        Log.e("==ERROR","ERROR "+streamId);
                        // TODO: 01/11/21
                        // Send reconnect  Event PluginEventError
                        delegate.onReconnectCall("auto",reconnectingId,"PluginEventError");
                        getListOFParticipant();
                    }

                    JSONArray publishers = plugin.optJSONArray("publishers");
                    if (publishers != null && publishers.length() > 0) {
                        for (int i = 0, size = publishers.length(); i <= size - 1; i++) {
                            JSONObject publisher = publishers.optJSONObject(i);
                            String feed = publisher.optString("id");
                            Log.e("===ID",feed);
                            String display = publisher.optString("display");
                           // kickUser(feed);
                            subscriberCreateHandle(feed, display);
                        }
                    }

                    String leaving = plugin.optString("leaving");
                    if (!TextUtils.isEmpty(leaving)) {
                        JanusHandle jhandle = feeds.get("leaving");
                        if (jhandle != null) {
                            jhandle.onLeaving.onJoined(jhandle);
                        }
                    }

                    JSONObject jsep = jo.optJSONObject("jsep");
                    if (jsep != null) {
                       handle.onRemoteJsep.onRemoteJsep(handle, jsep);
                    }

                } else if (janus.equals("detached")) {
                    handle.onLeaving.onJoined(handle);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void createSession() {
        String transaction = randomString(12);
        JanusTransaction jt = new JanusTransaction();
        jt.tid =  transaction;
        jt.success = new TransactionCallbackSuccess() {
            @Override
            public void success(JSONObject jo) {
                mSessionId = new BigInteger(jo.optJSONObject("data").optString("id"));
                Log.d("==SESSIONID",""+mSessionId);
                mHandler.post(fireKeepAlive);
                publisherCreateHandle();
            }
        };
        jt.error = new TransactionCallbackError() {
            @Override
            public void error(JSONObject jo) {
            }
        };
        transactions.put(transaction, jt);
        JSONObject msg = new JSONObject();
        try {
            msg.putOpt("janus", "create");
            msg.putOpt("transaction", transaction);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        mWebSocket.send(msg.toString());
    }

    private void publisherCreateHandle() {
        String transaction = randomString(12);
        JanusTransaction jt = new JanusTransaction();
        jt.tid = transaction;
        jt.success = new TransactionCallbackSuccess() {
            @Override
            public void success(JSONObject jo) {
                Log.d("==MYHANDLE",jo.toString());
                handleId = new BigInteger(jo.optJSONObject("data").optString("id"));
                JanusHandle janusHandle = new JanusHandle();
                janusHandle.handleId = new BigInteger(jo.optJSONObject("data").optString("id"));
                janusHandle.onJoined = new OnJoined() {
                    @Override
                    public void onJoined(JanusHandle jh) {
                        Log.d("==MYHANDLE", String.valueOf(jh.handleId));
                        delegate.onPublisherJoined(jh.handleId);
                    }
                };
                janusHandle.onRemoteJsep = new OnRemoteJsep() {
                    @Override
                    public void onRemoteJsep(JanusHandle jh,  JSONObject jsep) {
                        Log.d("==MYHANDLE","ONREMOTE "+ String.valueOf(jh.handleId));
                        delegate.onPublisherRemoteJsep(jh.handleId, jsep);
                    }
                };
                handles.put(janusHandle.handleId, janusHandle);
                 publisherJoinRoom();

            }
        };
        jt.error = new TransactionCallbackError() {
            @Override
            public void error(JSONObject jo) {
                // TODO: 01/11/21
                // Call reconecting Event
                delegate.onReconnectCall("auto",reconnectingId,"AttachPluginFailure-Publish");
            }
        };
        transactions.put(transaction, jt);
        JSONObject msg = new JSONObject();
        try {
            msg.putOpt("janus", "attach");
            msg.putOpt("plugin", "janus.plugin.videoroom");
            msg.putOpt("transaction", transaction);
            msg.putOpt("session_id", mSessionId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        mWebSocket.send(msg.toString());
    }

    public void publisherJoinRoom() {
        Log.d("===STREAM","STREAM"+streamId);
        JSONObject msg = new JSONObject();
        JSONObject body = new JSONObject();
        try {
            body.putOpt("request", "join");
            body.putOpt("room", roomId);
            body.putOpt("ptype", "publisher");
            body.putOpt("display", "Android webrtc");
            body.putOpt("id", streamId);

            msg.putOpt("janus", "message");
            msg.putOpt("body", body);
            msg.putOpt("transaction", randomString(12));
           msg.putOpt("session_id", mSessionId);
            msg.putOpt("handle_id", handleId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        mWebSocket.send(msg.toString());
        // TODO: 01/11/21
        // Set the Timer 10 sec to check publish request started
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                if (publishStartCountDownTimer == null) {
                    publishStartCountDownTimer = new CountDownTimer(BuildConfig.RECONNECTING_TIME, 1000) {
                        @Override
                        public void onTick(long millisUntilFinished) {

                        }

                        public void onFinish() {
                            delegate.onReconnectCall("auto",reconnectingId,"PublishNotStarted");
                        }

                    }.start();
                }

            }
        });
    }

    private void getListOFParticipant() {
        //Log.d("===STREAM",streamId);
        JSONObject msg = new JSONObject();
        JSONObject body = new JSONObject();
        try {
            body.putOpt("request", "listparticipants");
            body.putOpt("room", roomId);
            msg.putOpt("janus", "message");
            msg.putOpt("body", body);
            msg.putOpt("transaction", randomString(12));
            msg.putOpt("session_id", mSessionId);
            msg.putOpt("handle_id",handleId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        mWebSocket.send(msg.toString());
    }
    private void kickUser(String id) {
        //Log.d("===STREAM",streamId);
        JSONObject msg = new JSONObject();
        JSONObject body = new JSONObject();
        try {
            body.putOpt("request", "kick");
            body.putOpt("room", roomId);
            body.putOpt("id", id);
            msg.putOpt("janus", "message");
            msg.putOpt("body", body);
            msg.putOpt("transaction", randomString(12));
            msg.putOpt("session_id", mSessionId);
            msg.putOpt("handle_id", handleId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        mWebSocket.send(msg.toString());
    }

    public void destroyJanus(){
        if (mWebSocket != null) {
            JSONObject object = new JSONObject();
            try {
                object.put("janus", "destroy");
                object.put("transaction", "Destroy");
                object.put("session_id", mSessionId);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            mWebSocket.send(object.toString());
        }

    }

    public void leaveRoom(){
            //Log.d("===STREAM",streamId);
            JSONObject msg = new JSONObject();
            JSONObject body = new JSONObject();
            try {
                body.putOpt("request", "leave");
                body.putOpt("room", roomId);
                msg.putOpt("janus", "message");
                msg.putOpt("body", body);
                msg.putOpt("transaction", randomString(12));
                msg.putOpt("session_id", mSessionId);
                msg.putOpt("handle_id",handleId);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            mWebSocket.send(msg.toString());

    }

    public void publisherCreateOffer(final BigInteger handleId, final SessionDescription sdp) {
        JSONObject publish = new JSONObject();
        JSONObject jsep = new JSONObject();
        JSONObject message = new JSONObject();
        try {
            publish.putOpt("request", "configure");
            publish.putOpt("audio", true);
            publish.putOpt("video", true);

            jsep.putOpt("type", sdp.type);
            jsep.putOpt("sdp", sdp.description);

            message.putOpt("janus", "message");
            message.putOpt("body", publish);
            message.putOpt("jsep", jsep);
            message.putOpt("transaction", randomString(12));
            message.putOpt("session_id", mSessionId);
            message.putOpt("handle_id", handleId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        mWebSocket.send(message.toString());
    }

    public void subscriberCreateAnswer(final BigInteger handleId, final SessionDescription sdp) {
        JSONObject body = new JSONObject();
        JSONObject jsep = new JSONObject();
        JSONObject message = new JSONObject();

        try {
            body.putOpt("request", "start");
            body.putOpt("room", roomId);

            jsep.putOpt("type", sdp.type);
            jsep.putOpt("sdp", sdp.description);
            message.putOpt("janus", "message");
            message.putOpt("body", body);
            message.putOpt("jsep", jsep);
            message.putOpt("transaction", randomString(12));
            message.putOpt("session_id", mSessionId);
            message.putOpt("handle_id", handleId);
            Log.e(TAG, "-------------"  + message.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        mWebSocket.send(message.toString());
    }

    public void trickleCandidate(final BigInteger handleId, final IceCandidate iceCandidate) {
        JSONObject candidate = new JSONObject();
        JSONObject message = new JSONObject();
        try {
            candidate.putOpt("candidate", iceCandidate.sdp);
            candidate.putOpt("sdpMid", iceCandidate.sdpMid);
            candidate.putOpt("sdpMLineIndex", iceCandidate.sdpMLineIndex);

            message.putOpt("janus", "trickle");
            message.putOpt("candidate", candidate);
            message.putOpt("transaction", randomString(12));
            message.putOpt("session_id", mSessionId);
            message.putOpt("handle_id", handleId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        mWebSocket.send(message.toString());
    }

    public void trickleCandidateComplete(final BigInteger handleId) {
        JSONObject candidate = new JSONObject();
        JSONObject message = new JSONObject();
        try {
            candidate.putOpt("completed", true);

            message.putOpt("janus", "trickle");
            message.putOpt("candidate", candidate);
            message.putOpt("transaction", randomString(12));
            message.putOpt("session_id", mSessionId);
            message.putOpt("handle_id", handleId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void subscriberCreateHandle(final String feed, final String display) {
        String transaction = randomString(12);
        JanusTransaction jt = new JanusTransaction();
        jt.tid = transaction;
        jt.success = new TransactionCallbackSuccess() {
            @Override
            public void success(JSONObject jo) {
                JanusHandle janusHandle = new JanusHandle();
                janusHandle.handleId = new BigInteger(jo.optJSONObject("data").optString("id"));
                janusHandle.feedId = feed;
                janusHandle.display = display;
                janusHandle.onRemoteJsep = new OnRemoteJsep() {
                    @Override
                    public void onRemoteJsep(JanusHandle jh, JSONObject jsep) {
                        delegate.subscriberHandleRemoteJsep(jh.handleId, jsep);
                    }
                };
                janusHandle.onLeaving = new OnJoined() {
                    @Override
                    public void onJoined(JanusHandle jh) {
                        subscriberOnLeaving(jh);
                    }
                };
                handles.put(janusHandle.handleId, janusHandle);
                feeds.put(janusHandle.feedId, janusHandle);
                subscriberJoinRoom(janusHandle);
            }
        };
        jt.error = new TransactionCallbackError() {
            @Override
            public void error(JSONObject jo) {
            }
        };

        transactions.put(transaction, jt);
        JSONObject msg = new JSONObject();
        try {
            msg.putOpt("janus", "attach");
            msg.putOpt("plugin", "janus.plugin.videoroom");
            msg.putOpt("transaction", transaction);
            msg.putOpt("session_id", mSessionId);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        mWebSocket.send(msg.toString());
    }

    private void subscriberJoinRoom(JanusHandle handle) {

        JSONObject msg = new JSONObject();
        JSONObject body = new JSONObject();
        try {
            body.putOpt("request", "join");
            body.putOpt("room", roomId);
            body.putOpt("ptype", "subscriber");
            body.putOpt("feed", handle.feedId);

            msg.putOpt("janus", "message");
            msg.putOpt("body", body);
            msg.putOpt("transaction", randomString(12));
            msg.putOpt("session_id", mSessionId);
            msg.putOpt("handle_id", handle.handleId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        mWebSocket.send(msg.toString());
    }


    private void subscriberOnLeaving(final JanusHandle handle) {
        String transaction = randomString(12);
        JanusTransaction jt = new JanusTransaction();
        jt.tid = transaction;
        jt.success = new TransactionCallbackSuccess() {
            @Override
            public void success(JSONObject jo) {
                delegate.onLeaving(handle.handleId);
                handles.remove(handle.handleId);
                feeds.remove(handle.feedId);
            }
        };
        jt.error = new TransactionCallbackError() {
            @Override
            public void error(JSONObject jo) {
            }
        };

        transactions.put(transaction, jt);

        JSONObject jo = new JSONObject();
        try {
            jo.putOpt("janus", "detach");
            jo.putOpt("transaction", transaction);
            jo.putOpt("session_id", mSessionId);
            jo.putOpt("handle_id", handle.handleId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        mWebSocket.send(jo.toString());
    }

    private void keepAlive() {
        String transaction = randomString(12);
        JSONObject msg = new JSONObject();
        try {
            msg.putOpt("janus", "keepalive");
            msg.putOpt("session_id", mSessionId);
            msg.putOpt("transaction", transaction);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        mWebSocket.send(msg.toString());
    }

    private Runnable fireKeepAlive = new Runnable() {
        @Override
        public void run() {
            keepAlive();
            mHandler.postDelayed(fireKeepAlive, 30000);
        }
    };

    public void setDelegate(JanusRTCInterface delegate) {
        this.delegate = delegate;
    }

    private String randomString(Integer length) {
        final String str = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        final Random rnd = new Random();
        StringBuilder sb = new StringBuilder(length);
        for (int i = 0; i < length; i++) {
            sb.append(str.charAt(rnd.nextInt(str.length())));
        }
        return sb.toString();
    }
}
