package com.idfy.janus;



public interface JanusStateCallback {

    void onPublishSuccessful(boolean isPublishSuccess,boolean isRemoteStreamReceived,String iceState);

    void onIceSateFailed(String state);

    void onReconnectCall(String type,int roomId,String reason);

}
