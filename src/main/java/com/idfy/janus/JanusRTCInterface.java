package com.idfy.janus;


import org.json.JSONObject;

import java.math.BigInteger;

public interface JanusRTCInterface {

    void onPublisherJoined(BigInteger handleId);
    void onPublisherRemoteJsep(BigInteger handleId, JSONObject jsep);
    void subscriberHandleRemoteJsep(BigInteger handleId, JSONObject jsep);
    void onLeaving(BigInteger handleId);
    void onVideoPublish(boolean isVideoPublish);
    void onReconnectCall(String type,int roomId,String reason);
    void onSubscriberAttached();

}
